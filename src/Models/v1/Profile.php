<?php

namespace Inmovsoftware\ProfileApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    protected $table = "it_profiles";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'it_level_id', 'name', 'status'];



    public function Users()
    {
        return $this->hasMany('Inmovsoftware\UserApi\Models\V1\User', 'it_profiles_id', 'id');

    }



    public function scopefilterValue($query, $param)
    {
        $query->where($this->table. ".name", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->it_users = auth()->user()->id;
    }



}
