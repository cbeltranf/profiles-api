<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt' ])->group(function () {
Route::group([
    'prefix' => 'api/v1'
], function () {
    Route::apiResource('profile', 'Inmovsoftware\PositionApi\Http\Controllers\V1\ProfileController');
        });
});
