<?php
namespace Inmovsoftware\ProfileApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\ProfileApi\Models\V1\Profile;
use Inmovsoftware\ProfileApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProfileController extends Controller
{
    public function index(Request $request)
    {

    }


    public function store(Request $request)
    {

    }


    public function show(Profile $profile)
    {

        return response()->json($profile);

    }

    public function update(Request $request, Profile $profile)
    {


    }


    public function destroy(Profile $profile)
    {
        $item = $profile->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }

}
